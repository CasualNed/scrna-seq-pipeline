import argparse
import os
import pandas as pd
import re
import numpy as np
import imputationTrimmer
import matplotlib.pyplot as plt

outputDirectory = os.path.join(imputationTrimmer.imputationPath, "graphs")

def gerenateGraph(fpath):
    if os.stat(fpath).st_size == 0:
        print("%s is empty! Deleting..." % fpath)
        os.remove(fpath)
        return

    datapoints = pd.read_csv(fpath, header=None, index_col=0)
    #originalNumberOfCells = len(datapoints.columns)
    
    fig, ax = plt.subplots()
    
    geneDict = imputationTrimmer.positivePairings
    
    if imputationTrimmer.negativeSetPrefix in fpath:
        geneDict = imputationTrimmer.negativePairings
        
    # optional: when plotting gene-transcription pairings, drop all cells that don't have values for both gene OR transcription factor
    for pairing in geneDict.items():
        
        gene = pairing[0]

        if not gene in datapoints.index:
            continue
        #plottedCells = len(relevantDataframe.columns)
        
        for tf in pairing[1]:
            
            if not tf in datapoints.index:
                continue
            # prune complete dataframe to include only a single gene-tf pairing
            relevantDataframe = datapoints.loc[[gene, tf]]
            
            # drop columns with only 0
            relevantDataframe = relevantDataframe.loc[:, (relevantDataframe != 0).any(axis=0)]

            try:
                ax.scatter(relevantDataframe.loc[gene], relevantDataframe.loc[tf], color=np.random.rand(3,), label="%s-%s" % (gene, tf),
                       alpha=0.3, edgecolors='none')
            except ValueError:
                ax.scatter(relevantDataframe.loc[gene], relevantDataframe.loc[tf].iloc[0], color=np.random.rand(3,), label="%s-%s" % (gene, tf),
                       alpha=0.5, edgecolors='none')
    
    fileName = os.path.splitext(os.path.basename(fpath))[0]
    
    plotTitle = fileName.replace("output", "")
    plotTitle = re.sub(r'[\_\:\-\d_]+', '', plotTitle.replace("-", " "))
    
    plt.title(plotTitle)
    
    ax.legend()
    ax.set_ylabel("Transcription Factor")
    ax.set_xlabel("Gene")
    
    outputFile = os.path.join(outputDirectory, fileName)
    plt.savefig(outputFile)

def main(args):
    
    if not os.path.isdir(outputDirectory):
        os.mkdir(outputDirectory)
    
    for file in os.listdir(imputationTrimmer.imputationPath):
        
        if not file.lower().endswith(".csv"):
            continue
            
        if file.lower().startswith(imputationTrimmer.positiveSetPrefix) or file.lower().startswith(imputationTrimmer.negativeSetPrefix):

            whoa = os.path.splitext(file)[0]
            outputFile = os.path.join(outputDirectory, os.path.splitext(file)[0] + '.png')
            
            if os.path.isfile(outputFile):
                print("already exists: %s" % outputFile)
                continue
            else:
                readIn = os.path.join(imputationTrimmer.imputationPath, file)
                print("reading in %s" % readIn)
                gerenateGraph(readIn)
        
        #plt.scatter(positiveImputedDataset)
        #plt.show()

if __name__=='__main__':
    parser = argparse.ArgumentParser(description="Takes a directory of imputation files output by imputationTrimmer, then produces a corresponding set of scatterplots")
    args = parser.parse_args()
    main(args)
    
