#!/usr/bin/env Rscript

#source('/home/s8anhegg/Tools/knn-smoothing/knn_smooth.R')
#setwd("/home/s8anhegg/datasets/downsampled")

source('/home/rstudio/shared_pipeline_data/Tools/knn-smoothing/knn_smooth.R')

main <- function(inputFile, outputDirectory){

data <- read.table(inputFile,header=TRUE, sep = "\t",row.names=1, check.names=F)

knn_smoothing_output <- knn_smoothing(data, k=20) 

fileName = paste(gsub(".txt", "", inputFile), "_kNN-smoothing_output", sep="")
fileName = paste(fileName, gsub(" ", "_", Sys.time()), sep="_")
fileName = paste(fileName, ".tsv", sep="")

setwd(outputDirectory)
write.table(knn_smoothing_output, fileName, sep="\t", col.names=F, row.names=T, quote=F)

}

locations = commandArgs(trailingOnly = TRUE)
inputFile = locations[1] 
outputDirectory = locations[2]

if(file.exists(inputFile) && file.exists(outputDirectory))
  main(inputFile, outputDirectory)

