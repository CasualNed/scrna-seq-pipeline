#!/usr/bin/env Rscript

library(data.table)

#source('/home/s8anhegg/run-scripts/Rscripts/frequentlyUsedFunctions.R')
source('/home/rstudio/shared_pipeline_data/run-scripts/Rscripts/frequentlyUsedFunctions.R')

#originals = file.path("/home/s8anhegg/datasets/imputed-outputs")
#outputDirectory = file.path("/home/s8anhegg/datasets/flipped-output")

originals = file.path("/home/rstudio/shared_pipeline_data/Datasets/genetfc-example-input/shortened/unimputed")
outputDirectory = file.path("/home/rstudio/shared_pipeline_data/Datasets/genetfc-example-input/shortened/output/")

filenames = list.files(path = originals, full.names = TRUE, recursive = TRUE)
for( i in 1:length(filenames))
{
  data <- loadFile(filenames[i])
  
  dimensions = dim(data)
  
  if (dimensions[2] > dimensions[1]){ # if there are more cells than genes
    print(paste("flipping", filenames[i]))
    outputName = paste(file.path(outputDirectory, baseNameNoExtension(filenames[i])), ".csv", sep="")
    write.table(data.frame(t(data)), outputName, sep=",", col.names=T, row.names=T)
  }
  
}
