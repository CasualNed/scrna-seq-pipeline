library(stringr)

baseNameNoExtension <- function(file){
  return(tools::file_path_sans_ext(basename(file)))
}

loadFile <- function(path){
  
  if (tools::file_ext(path) == "csv")
    readIn = read.csv(path, row.names = 1)
  else
    readIn = read.table(path, sep = "\t", row.names=1)
  
  # deepImpute outputs a (cell x gene) matrix instead of a (gene x cell)
  # if (grepl("deepImpute", basename(path), ignore.case=TRUE)) 
  # readIn = t(readIn)
  
  # dca actually retains the cell names
  # if (grepl("dca", basename(path), ignore.case=TRUE)) {
  # colnames(readIn) = as.character(readIn[1, ])
  # readIn <- readIn[-1,]
  # }
  
  # dimensions = dim(readIn)
  # print(paste("(", Sys.time(), ")"," Loaded ", basename(path), ", which details ", dimensions[1], " rows and ", dimensions[2], " columns" , sep=""))
  # print("Example gene names: ")
  # print(rownames(readIn)[1:3])
  # print("Example cell names:")
  # print(colnames(readIn)[1:3])
  
  return(readIn)
}

getUniqueName <- function(allFileNames, currentNameIndex){
  
  wordBag = vector(mode="character")
  
  allFileNames = baseNameNoExtension(allFileNames)
  
  if (length(allFileNames) == 0)
    return(NULL)
  
  # split all file names according to underscore, then append to wordBag
  for (i in 1:length(allFileNames)){
    
    if (i == currentNameIndex)
      next
    
    wordBag = c(wordBag, str_split(allFileNames[i], "_|\ ")[[1]])  
  }
  
  # remove all timestamps 
  fileNameSplit = str_split(allFileNames[currentNameIndex], "_|\ ")[[1]]
  pattern <- "[0-9]+" #"\\d+" 
  wordBag = wordBag[!str_detect(wordBag, pattern)] # pattern matches numbers
  fileNameSplit = fileNameSplit[!str_detect(fileNameSplit, pattern)]
  
  #fileName = allFileNames[currentNameIndex]
  #answer = paste(setdiff(fileNameSplit, wordBag), collapse=" ")
  answer = setdiff(fileNameSplit, wordBag)
  
  if (identical(answer, character(0)))
    return(allFileNames[currentNameIndex])
  
  # return any word(s) unique to the current file name
  return(answer)
}

# This function assumes that there is a one-to-one ratio between input and output
# So, don't use this if an input results in multiple outputs
getNovelInputs <- function(inputDirectory, outputDirectory, outputTag){
  
  inputs = dir(inputDirectory, full.names = TRUE)
  outputs = dir(outputDirectory, recursive = TRUE, full.names = TRUE)
  toRemove = c()
  
  if (identical(inputs, character(0))){
    print("No inputs found")
    return(NULL)
  }
  
  if (identical(outputs, character(0))){
    print("No pre-existing outputs")
    return(inputs)
  }
  
  for (i in 1:length(inputs)){
    
    organ = getUniqueName(inputs, i)
    
    if(is.null(organ)){
      print(paste("no unique name found for", inputs[i]))
      next
    }
    
    for (j in 1:length(outputs)){
      
      searchStrings = c(outputTag, organ)
      
      if(all(sapply(searchStrings, grepl, baseNameNoExtension(outputs[j]), ignore.case=TRUE))){
        toRemove = c(toRemove, i)
        break
      }
    }
  }
  
  if (length(toRemove) > 0){
    print(paste(length(inputs[toRemove]), "outputs already exists. Skipping these files..."))
    return(inputs[-toRemove])
  }
  
  return(inputs)
}

getMatchingFiles <- function(allFiles, searchTerms){
  
  matchedFiles = c()
  
  if (length(allFiles) == 0)
    return(matchedFiles)
  
  for (i in 1:length(allFiles)){
    
    #if (grepl(tolower(searchTerms), filenameSplit))
    if(all(sapply(searchTerms, grepl, baseNameNoExtension(allFiles[i]), ignore.case=TRUE)))
      matchedFiles = c(matchedFiles, allFiles[i])
  }
  
  
  return(matchedFiles)
  
}