#!/usr/bin/env Rscript
library(stringr)
library(reshape2)
library(ggplot2)
library(dplyr)
library(forcats)

#source('/home/s8anhegg/run-scripts/Rscripts/frequentlyUsedFunctions.R')
#source('/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/run-scripts/Rscripts/frequentlyUsedFunctions.R')
source('/home/rstudio/shared_pipeline_data/run-scripts/Rscripts/frequentlyUsedFunctions.R')

loadFile <- function(path){
  
  readIn = read.table(path, sep = "\t")
  
  dimensions = dim(readIn)
  print(paste("(", Sys.time(), ")"," Loaded ", basename(path), ", which details ", dimensions[2], " bulk correlations", sep=""))

  return(readIn)
}


getColumnWithNameContaining <- function(name, dataframe){

  for (i in 1:ncol(dataframe)){
    
    columnNameSplit = tolower(str_split(colnames(dataframe)[i], "_"))
    
    if (grepl(tolower(name), columnNameSplit))
      return(dataframe[i])
    
  }
  
  return(NULL)
}

calculatePseudobulkCorrelation <- function(bulkAverages, uninputedAverages, organName){
  
  selectedBulkColumn = getColumnWithNameContaining(organName, bulkAverages)
  selectedUnimputedColumn = getColumnWithNameContaining(organName, uninputedAverages)
  
  if (is.null(selectedBulkColumn) | is.null(selectedUnimputedColumn))
    return(NULL)
  
  # drop NA
  selectedBulkColumn = selectedBulkColumn[!is.na(selectedBulkColumn), ,drop=FALSE]
  selectedUnimputedColumn = selectedUnimputedColumn[!is.na(selectedUnimputedColumn), ,drop=FALSE]
  
  # set gene names to upper-case for string matching
  rownames(selectedBulkColumn) = toupper(rownames(selectedBulkColumn))
  rownames(selectedUnimputedColumn) = toupper(rownames(selectedUnimputedColumn))
  
  # drop non-shared genes
  toKeep = intersect(rownames(selectedUnimputedColumn), rownames(selectedBulkColumn))
  selectedBulkColumn = selectedBulkColumn[toKeep, , drop=FALSE] # drop=TRUE is default, and results in a vector
  selectedUnimputedColumn = selectedUnimputedColumn[toKeep, , drop=FALSE]
  
  # make alphabetical according to gene names
  selectedBulkColumn = selectedBulkColumn[order(rownames(selectedBulkColumn)), , drop=FALSE]
  selectedUnimputedColumn = selectedUnimputedColumn[order(rownames(selectedUnimputedColumn)), ,drop=FALSE]
  
  corr <- cor.test(selectedBulkColumn[,1], selectedUnimputedColumn[,1], method = 'spearman', exact=FALSE)
  
  return(corr$estimate)
  
}

# reads in a directory of bulk-correlation evaluations, and outputs a directory of graphs comparing each
main <- function(bulkRnaSeqPath, imputationEvaluatedDirectory, unimputedAveragesPath){
  
  # only used for grouping correlation files according to organ
  bulk = read.csv(bulkRnaSeqPath, row.names = 1, header= TRUE)
  
  # necessary for plotting pseudobulk average
  unimputedAverages = read.table(unimputedAveragesPath, row.names = 1, header= TRUE)

  filenames = list.files(imputationEvaluatedDirectory, full.names = TRUE, recursive = TRUE)
  
  mergedMatrix <- matrix(ncol=2, nrow=0)
  colnames(mergedMatrix) <- c("cor", "method")
  
  # iterate through all organs, generating graphs for each
  for( i in 1:length(colnames(bulk)))
    
  {
    matchedFiles = getMatchingFiles(filenames, colnames(bulk)[i])
    
    print(paste("(", Sys.time(), ") ", colnames(bulk)[i], " has ", length(matchedFiles), " matches...", sep=""))
    if (length(matchedFiles) > 0)
      print(matchedFiles)
    else
      next
    
    pseudobulkCorrelation = calculatePseudobulkCorrelation(bulk, unimputedAverages, colnames(bulk)[i])
    
    # toPlot = c()
    
    # TODO: adopt the mergedMatrix approach to generating the plots, add facet_grid?
    for (j in 1:length(matchedFiles)){
    # load in, then make row names upper-case 
    currentDataset = subset(melt(t(loadFile(matchedFiles[j]))), select=-1)
    colnames(currentDataset) = c("method", "cor")
    
    currentDataset$method = getUniqueName(matchedFiles, j)[1]
    
    if (tolower(currentDataset$method[1]) == "knn")
      currentDataset$method = "kNN-smoothing"
    
    mergedMatrix = rbind(mergedMatrix, currentDataset)
    
    
    # toPlot = c(toPlot, 
    #            geom_violin(data=currentDataset, aes(x=method, y = cor)), 
    #            geom_jitter(data=currentDataset, aes(x=method, y = cor, colour=method), width=0.4, stroke = 0.5, alpha=0.01))
    
    }
    
    #if (length(toPlot) == 0)
    #  next
    
    mergedMatrix = data.frame(mergedMatrix)
    mergedMatrix = mergedMatrix %>% mutate(method = fct_relevel(method, "ALRA", "DCA", "DeepImpute", "kNN-smoothing", "MAGIC", "SAVER", "SAVER-X", "scIGANs", "scVI"))
    
    # initialize with first 
    # p2 <- ggplot(data=toPlot[[1]]$data, aes(x=factor(method), y = cor)) + geom_violin() + coord_flip() + theme_classic() + xlab('') + ylab(paste('Pseudobulk correlation =', format(round(pseudobulkCorrelation, 2), nsmall = 2)))
     
    
    # add the rest
    # for (j in 2:length(toPlot))
    # p2 = p2 + toPlot[[j]]
    
    p2 <- ggplot() + geom_violin() + coord_flip() + xlab('') + ylab(paste('Pearson Correlation. Pseudobulk correlation =', format(round(pseudobulkCorrelation, 2), nsmall = 2))) # + theme_classic() + scale_color_manual(values=wes_palette(name="GrandBudapest2", type="continuous"))
    p2 = p2 + geom_violin(data=mergedMatrix, aes(x=method, y = cor))
    p2 = p2 + geom_jitter(data=mergedMatrix, aes(x=method, y = cor, colour=method), width=0.4, stroke = 0.5,  alpha=0.01) 
    p2 = p2 + ggtitle(paste(colnames(bulk)[i], "Tissue Bulk Correlation")) + theme(plot.title = element_text(hjust = 0.5)) # this theme centers the title
    # p2 = p2 + ggtitle(paste(plotname, "Downsampling Evaluation")) + theme(plot.title = element_text(size = 10))
    p2 = p2 + guides(colour= "none") # disables colour legend
    p2 = p2 + facet_grid(method ~ ., scales = "free", space = "free")
    p2 = p2 + theme(strip.text.y = element_blank()) # removes unnecessary labels
    
    # add pseudobulk benchmark line
    p2 = p2 + geom_hline(yintercept=pseudobulkCorrelation, color="red", linetype="dashed")
    p2 = p2 + guides(colour= "none") # disable colour legend
    # p2 = p2 + facet_grid(supp ~ .,  scales = "free", space = "free")
    
    # add label for benchmark line, rounded to 2 decimal places
    # p2 = p2 + scale_y_continuous(breaks = c(pretty_br,pseudobulkCorrelation), labels = format(round(pseudobulkCorrelation, 2), nsmall = 2))
    
     setwd("/home/rstudio/shared_pipeline_data/")
     print(paste("(", Sys.time(), ")", " Done. Printing...", sep=""))
     outputName = paste(colnames(bulk)[i], "-bulk-correlation-", gsub("\ ", "_", Sys.time()), ".png", sep="")
     ggsave(outputName, dpi=1000)
    }
  }


#bulkRnaSeqLocation = "/home/s8anhegg/datasets/bulkRnaProfile.csv"
#scRnaSeqDirectory =  "/home/s8anhegg/datasets/imputed-outputs/"

bulkRnaSeqPath = "/home/rstudio/shared_pipeline_data/Datasets/bulk-RNA/bulkRnaProfile.csv"
unimputedAveragesPath = "/home/rstudio/shared_pipeline_data/Datasets/bulk-RNA/home_s8anhegg_datasets_cleaned_comma-spaced_average.tsv"
imputationEvaluatedDirectory = "/home/rstudio/shared_pipeline_data/Datasets/bulk-RNA/second-run-bulk-correlation"

main(bulkRnaSeqPath, imputationEvaluatedDirectory, unimputedAveragesPath)