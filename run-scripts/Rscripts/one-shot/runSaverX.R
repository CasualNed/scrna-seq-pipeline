#!/usr/bin/env Rscript

library(SAVERX)
library(reticulate)
library("pryr")
use_condaenv("saverx", required = TRUE)

main <- function(inputFile, outputDirectory){
  
  file <- saverx(inputFile, is.large.data = T, clearup.python.session = F)
  denoised.data <- readRDS(file) 
  
  setwd(outputDirectory)
  
  fileName = paste(gsub(".csv", "", inputFile), "_SAVER-X_output", sep="")
  fileName = paste(fileName, gsub(":", ".", gsub(" ", "_", Sys.time())), sep="_")
  fileName = paste(fileName, ".tsv", sep="")
  write.table(denoised.data[[1]], fileName, sep="\t", col.names=F, row.names=T, quote=F)
}

locations = commandArgs(trailingOnly = TRUE)
inputFile = locations[1] 
outputDirectory = locations[2]

if(file.exists(inputFile) && file.exists(outputDirectory))
  main(inputFile, outputDirectory)
