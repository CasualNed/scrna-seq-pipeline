#!/usr/bin/env Rscript

library(stringr)
library(data.table)

loadFile <- function(path){
  
  if (tools::file_ext(path) == "csv")
    readIn = read.csv(path, row.names = 1)
  else
    readIn = read.table(path, row.names=1)
  
  # deepImpute outputs a (cell x gene) matrix instead of a (gene x cell)
  if (grepl("deepImpute", basename(path), ignore.case=TRUE)) 
    readIn = t(readIn)
  
  # dca actually retains the cell names
  if (grepl("dca", basename(path), ignore.case=TRUE)) {
    colnames(readIn) = as.character(readIn[1, ])
    readIn <- readIn[-1,]
  }
  
  dimensions = dim(readIn)
  print(paste("(", Sys.time(), ")"," Loaded ", basename(path), ", which details ", dimensions[1], " genes and ", dimensions[2], " cells" , sep=""))
  print("Example gene names: ")
  print(rownames(readIn)[1:3])
  print("Example cell names:")
  print(colnames(readIn)[1:3])
  
  return(readIn)
}



main <- function(inputFilePath, outputDirectory){

  toRun = list.files(path = inputFilePath, full.names = TRUE, recursive = TRUE)
  completeDataframe = NULL
  
  for( i in 1:length(toRun)){
    
    dataset = loadFile(toRun[i])
    rowNames = rownames(dataset)
    
    dataset[is.na(dataset)] = 0
    
    averaged = as.data.frame(rowMeans(dataset))          
    colnames(averaged) = basename(toRun[i])

    # can't merge an empty dataframe -- initialize if NULL instead
    if (is.null(completeDataframe))
      completeDataframe = averaged
    else{
      completeDataframe = merge(completeDataframe, averaged, by='row.names', all = T)
      
      # instead of renaming the actual row names, merge outputs a new column with the name "Row.namesx"
      # where x is the number of calls
      rownames(completeDataframe) = completeDataframe[["Row.names"]]
      completeDataframe = subset(completeDataframe, select=-Row.names)
    }
    print(paste("(", Sys.time(), ")", " Appended average of ", toRun[i], sep=""))
  }
  
    print(paste("(", Sys.time(), ")", " Done. Printing correlation...", sep=""))
    outputName = substring(paste(gsub("/", "_", inputFilePath), "_average.tsv", sep=""), 2)
    
    setwd(outputDirectory)
    write.table(completeDataframe, outputName, sep="\t", col.names=T, row.names=T)
  }

inputFilePath = "/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/toy-inputs"
outputDirectory = "/tmp"

main(inputFilePath, outputDirectory)
