#!/bin/bash

for file in /home/rstudio/shared_pipeline_data/datasets/downsampled/comma-spaced/*
do
	BASENAME=$(basename -s .csv $file)
	OUTPUTDIRECTORY=/home/rstudio/shared_pipeline_data/outputs/dca_$BASENAME
	mkdir $OUTPUTDIRECTORY
	/opt/miniconda3/envs/dca/bin/dca $file $OUTPUTDIRECTORY
done

