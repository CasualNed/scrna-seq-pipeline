import numpy as np
import scvi as scvi
import pandas as pd
import sys
import os
from datetime import datetime
from scvi.dataset import CsvDataset

inFile = sys.argv[1]
outputDirectory = sys.argv[2]
local_csv_dataset = CsvDataset(inFile)

from scvi.models import *
from scvi.inference import UnsupervisedTrainer
vae = VAE(local_csv_dataset.nb_genes) # vae = autoencoder
unsupervisedTrainer = UnsupervisedTrainer(vae, local_csv_dataset, use_cuda=True)
unsupervisedTrainer.train(400) # train for 400 epochs

full = unsupervisedTrainer.create_posterior(unsupervisedTrainer.model, gene_dataset=local_csv_dataset, indices=np.arange(len(local_csv_dataset)))
imputed_values = full.sequential().imputation() #outputs a squeeze matrix -- https://docs.scvi-tools.org/en/0.6.3/_modules/scvi/inference/posterior.html#Posterior
dataframe=pd.DataFrame(imputed_values, columns=local_csv_dataset.gene_names).T

# 3) print imputed values
os.chdir(outputDirectory)
justFileName = inFile.split("/")[-1]
justFileName = justFileName.replace(".csv", "")
now = datetime.now()
justFileName += "_scVI_" + now.strftime("%m-%d-%Y_%H:%M:%S")  + ".csv"
dataframe.to_csv(outputDirectory + justFileName, index=True, header=False)
