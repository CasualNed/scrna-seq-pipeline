import pandas as pd
from scipy import sparse
import os
import datetime

inputPath = "/share/data/DATASETS/2019-Stanford-Single-Cell-Aging"
outputPath = "/home/student/aheggen"

def format_bytes(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'k', 2: 'm', 3: 'g', 4: 't'}
    while size > power:
        size /= power
        n += 1
    return size, power_labels[n]+'b'

for file in os.listdir(inputPath):
    
    if not file.lower().endswith("annotated.txt"):
        continue
    
    fname= os.path.join(inputPath, file)
    matrixOutput = os.path.join(outputPath, os.path.splitext(file)[0] + '_matrix.npz')
    
    if os.path.isfile(matrixOutput):
        print("already exists: %s" % matrixOutput)
        continue
    
    print("Reading in \"%s\" " % (fname))
    originalSize = format_bytes(os.path.getsize(fname))
    print("Start time: %s" % datetime.datetime.now().time())
    print("Original size: %1.1f%s " % (originalSize[0], originalSize[1]))
    
    
    originalDataset = pd.read_csv(fname, index_col=0, sep='\t')
    data = originalDataset.fillna(0)
    
    # write to file
    columnOutput = os.path.join(outputPath, os.path.splitext(file)[0] + '_columns.txt')
    rowOutput = os.path.join(outputPath, os.path.splitext(file)[0] + '_rows.txt')
    
    
    data.columns.values.tofile(columnOutput,sep=',')
    data.index.values.tofile(rowOutput,sep=',')
    
    wow = sparse.csr_matrix(data.values)
    sparse.save_npz(matrixOutput, wow)
    
    compressedSize = format_bytes(os.path.getsize(matrixOutput))
    print("Compressed size: %1.1f%s " % (compressedSize[0], compressedSize[1]))
