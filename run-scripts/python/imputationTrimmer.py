import argparse
import os
import pandas as pd
import matplotlib.pyplot as plt

imputationPath = "/home/s8anhegg/outputs"

positivePairings = {
    # key = gene, value = transcription factor(s)
    "CDKN2A": ["MITF", "FOXM1"],
    "TNF": ["ETS1"],
    "LMNB1": ["SNAI2"],
    "IL10": ["SP1"],
    "IL1B": ["SP1"],
    "BST1": ["HNF1A", "CEBPB"],
    "STAT6": ["PARP14", "IL4"],
    "ITGAX": ["SP1"],
    "ITGAM": ["GABPA", "GABPB1"]
    } 

negativePairings = {
    "ITGAX": ["SPI1"],
    "SULF2": ["ETS1"],
    "ANGPTL2": ["SNAI2"],
    "SIRT4": ["SP1"],
    "NNMT": ["SP1"],
    "SIRT5": ["HNF1A", "CEBPB"],
    "BCL2L2": ["PARP14", "IL4"],
    "NMNAT1": ["SP1"],
    "NAMPT": ["POU2F2"],
    "BIRC5": ["PARP6"],
    "IGFBP2": ["HSP27"]
    } 

positiveSetPrefix = "positive-regulation-"
negativeSetPrefix = "negative-regulation-"

def getAllValidGenes(validGenes, geneDict, allGenes):
    
    answer = []
    
    for k in validGenes:
        
        pairExists = False
        
        for j in geneDict[k]:
            if j in allGenes:
                answer.append(j)
                pairExists = True
        
        if pairExists:
            answer.append(k)
    
    return answer
     
def performTrimming(pairingDict, originalDataset):    
    possibleGenes = [i for i in pairingDict.keys() if i in originalDataset.index]
    validGenesAndTfs = getAllValidGenes(possibleGenes, pairingDict, originalDataset.index)
    
    validGenes = [i for i in possibleGenes if i in validGenesAndTfs]
    print("include %i pairing(s), totalling %i genes"  % (len(validGenes), len(validGenesAndTfs)))
    
    return originalDataset.loc[validGenesAndTfs]
        
def trimToGoldStandard(originalDataset, usePositivePairings):
    
    if usePositivePairings:
        print("positive matches ", end = '')
        originalDataset = performTrimming(positivePairings, originalDataset)
    else:
        print("negative matches ", end = '')
        originalDataset = performTrimming(negativePairings, originalDataset)
          
    return originalDataset

def generateTrimmedFile(fname, imputedDataset, trimmedFname):
    
    usePositive=True
    
    if negativeSetPrefix in trimmedFname:
        usePositive = False
    
    imputedDataset = trimToGoldStandard(imputedDataset, usePositive)
    
    imputedDataset = imputedDataset.loc[:, (imputedDataset != 0).any(axis=0)]
    imputedDataset.dropna(inplace=True, axis=1, how='all')
    imputedDataset.to_csv(trimmedFname, header=False)


def main(args):
    
    for file in os.listdir(imputationPath):
               
        if file.lower().startswith(positiveSetPrefix) or file.lower().startswith(negativeSetPrefix):
            continue
        
        # generate positive and negative trimmed files if they don't exist
        fname= os.path.join(imputationPath, file)
        positiveTrimmedFile = os.path.join(imputationPath, positiveSetPrefix + file)
        negativeTrimmedFile = os.path.join(imputationPath, negativeSetPrefix + file)
        
        if not os.path.isfile(positiveTrimmedFile) or not os.path.isfile(negativeTrimmedFile):
            print("reading in %s" % fname)
            if file.lower().endswith(".csv"):
                originalDataset = pd.read_csv(fname, index_col=0)
            else:
                originalDataset = pd.read_csv(fname, index_col=0, sep='\t')
            
        else:
            print("gold standards already exist for %s" % fname)
        
        if not os.path.isfile(positiveTrimmedFile): # trim to gold standard, then write to file
            generateTrimmedFile(fname, originalDataset, positiveTrimmedFile)
        #else: # use trimmed file 
            generateTrimmedFile(fname, originalDataset, negativeTrimmedFile)
        #else: 
        #    negativeImputedDataset = pd.read_csv(negativeTrimmedFile, header=None, index_col=0)

if __name__=='__main__':
    parser = argparse.ArgumentParser(description="Takes a directory of imputation files, then produces a \"trimmed\" version of each of these files, using the name <type>-regulation-<original-file-name>")
    args = parser.parse_args()
    
    main(args)
    
   
    
"""
Significantly increasing with age:

    • Cdkn2a, encodes a tumor suppressor protein that seems to be highly studied
        ◦ MITF -- present
        ◦ FOXM1-- present
    • E2f2
    • Tnf, encodes a generalist protein that regulates immune cells, inhibits tumorigenesis as well as viral replication
        ◦ ETS1 -- present
    • Lmnb1, encodes a generalist protein “thought to be involved in nuclear stability, chromatin structure, and gene expression”
        ◦ SNAI2 (aka SLUG, “recruited by LMNB1 gene promoter mostly when chondrocytes undergo de-differentiation or OA degeneration”) – present
        ◦ P63 – not present
    • Il10, encodes an anti-inflammatory cytokine
        ◦ SP1 -- present
    • Il1b, encodes a generalist cytokine involved in inflammation, and the cell cycle
        ◦ SP1 (“For keratinocytes, a specificity protein-1 (SP-1) binding site within the core promoter was identified as essential for steady state IL-1β mRNA expression”) – present 
    • Bst1, aka CD157, encodes an enzyme involved in B-cell growth
        ◦ HNF1A (aka TCF1) – present
        ◦ CEBPB (aka NF-IL6) - present
    • Irg1 is not listed in the mouse aorta dataset
    • Parp14 enhances STAT6 transcription in the presence of IL-4, and inhibits STAT6 transcription otherwise: “In the absence of IL-4, PARP-14 was found to be bound to STAT6 responsive promoters, and functioned as a transcriptional repressor by recruiting HDAC 2 and 3. In the presence of IL-4 the catalytic activity of PARP-14 modified the HDACs and the repressive complex was displaced from the promoter to activate transcription [25]. PARP-14 is required for STAT6-dependent gene expression in B cells and T helper cells”
        ◦ STAT6 – present
        ◦ IL4 - present
    • Itgax (aka CD11c) is an transmembrane protein expressed on dendritic cells that induces cellular activation (e.g., neutrophil respiratory burst – releasing reactive oxygen species)
        ◦ Unable to download the full pdf of “PU.1 negatively regulates the CD11c integrin gene promoter through recognition of the major transcriptional start site”
        ◦ SP1 – present
        ◦ SPI1 (negative regulator) – present 
    • Itgam (aka CD11b) is a subunit of a leukocyte membrane protein
        ◦ Unable to download the full pdf of “The heteromeric transcription factor GABP activates the ITGAM/CD11b promoter and induces myeloid differentiation”
        ◦ GABPA – present
        ◦ GABPB1 – present 

Significantly decreasing with age:



    • Sulf2 encodes an extracellular protein that assists growth factors in binding to their receptors
        ◦ p53 (aka TRP53) -- present
    • Angptl2 (aka ARP2) encodes a subunit of a protein that assembles branched actin
        ◦ can’t be found
    • Sirt4 encodes a mitochondrial protein that removes post-translational acyl modifications
        ◦ I couldn’t download the full version of “DNA Methylation and Transcription Factors Competitively Regulate SIRT4 Promoter Activity in Bovine Adipocytes: Roles of NRF1 and CMY
        ◦ NRF1 (transcriptional repressor) – present
        ◦ CMYB (aka MYB) -- present
    • Sirt3
        ◦ NRF1 (transcriptional repressor) – present
        ◦ CMYB (aka MYB) -- present
    • Sirt3
        ◦ not found
    • Nnmt
        ◦ HNF-1beta (aka HNF1b) -- present
    • Sirt5
        ◦ couldn’t download the full version of “Competitive regulation by transcription factors and DNA methylation in the bovine SIRT5 promoter: Roles of E2F4 and KLF6”
        ◦ E2F4 (activator)  -- present
        ◦ KLF6 (inhibitor) -- present
    • Bcl2l2
        ◦ WT1 (activator) – present 
    • Nmnat1 encodes a subunit of a protein called “Wallerian degeneration slow”. Nmnat1 “interacts with SIRT1 at target gene promoters, regulating transcription of genes important for neuronal function ... SIRT1 directly interacts with DNA binding transcription factors and coregulators and is recruited to gene promoters through these interactions.Many of these same factors are direct targets of deacetylation by SIRT1, including p53, Ku70, NF-#B, the FOXO family of tran-scription factors, liver X receptor, estrogen receptor#, SOX9,PGC-1#, and p300”
        ◦ p53 (aka TRP53) – present
        ◦ ku70 (aka XRCC6) – present 
    • Nampt
        ◦ pou2f2 – present 
        ◦ nampt-as (“nampt-anti sense”) – not present
    • Parp6
        ◦ downregulates birc5 – present
    • Igfbp2
        ◦ “Hsp27 (aka hspb1) regulates IGFBP2 expression through indirect gene transcription” – present 
    • Parp3 encodes a protein that performs post-translational modifications that affect a wide range of processes (e.g., genome integrity, cell death). 
        ◦ “Thus, and in contrast with PARP1, PARP3 does not appear to serve a major regulatory role in transcription, at least within our experimental system.” -- PARP3 is a promoter of chromosomal rearrangements and limits G4 DNA
        ◦ couldn’t find anything about parp3’s transcription factor
        
    """
