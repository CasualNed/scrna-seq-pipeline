#!/usr/bin/env Rscript

library(SAVER)
library(stringr)
source('/home/s8anhegg/run-scripts/Rscripts/frequentlyUsedFunctions.R')
#source('/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/run-scripts/Rscripts/frequentlyUsedFunctions.R')

main <- function(inputDirectory, outputDirectory){
  
filenames <- getNovelInputs(inputDirectory, outputDirectory, "_SAVER_")

for( i in 1:length(filenames)){

  print(paste("running", filenames[i]))

  data <- loadFile(filenames[i])

  corex_output <- saver(data, ncores=4) # thor has 80 cores, but the amount of memory necessary to run all 80 is too much
  fileName = file.path(outputDirectory, paste(baseNameNoExtension(filenames[i]), "_SAVER_", sep =""))
  fileName = paste(fileName, gsub(" ", "_", Sys.time()), sep="_")
  fileName = paste(fileName, ".tsv", sep="")
  write.table(corex_output[[1]], fileName, sep="\t", col.names=F, row.names=T, quote=F)

  fileName = file.path(outputDirectory, paste(baseNameNoExtension(filenames[i]), "_SAVER_standard_error_output", sep =""))
  fileName = paste(fileName, gsub(" ", "_", Sys.time()), sep="_")
  fileName = paste(fileName, ".tsv", sep="")
  write.table(corex_output[[2]], fileName, sep="\t", col.names=F, row.names=T, quote=F)
  
  }
}

#inputDirectory <- "/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/14Dec2021_datasets-structure/downsampled/comma-spaced/"
#outputDirectory <- "/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/14Dec2021_datasets-structure/downsampled-rescue/saver"

inputDirectory <- "/home/s8anhegg/datasets/downsampled/saver-input/"
outputDirectory <- "/home/s8anhegg/datasets/downsampled-rescue/saver/"

main(inputDirectory, outputDirectory)
