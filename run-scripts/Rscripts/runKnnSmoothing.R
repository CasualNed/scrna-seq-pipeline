#!/usr/bin/env Rscript

source('/home/s8anhegg/Tools/knn-smoothing/knn_smooth.R')

setwd("/home/s8anhegg/datasets/downsampled")

filenames = dir(pattern="*.txt$")
for( i in 1:length(filenames) )
{
  
  setwd("/home/s8anhegg/datasets/downsampled")
  data <- read.table(filenames[i],header=TRUE, sep = "\t",row.names=1, check.names=F)
  
  knn_smoothing_output <- knn_smoothing(data, k=20) 
  
  setwd("/home/s8anhegg/outputs/")
  fileName = paste(gsub(".txt", "", filenames[i]), "_knn_smoothing_output", sep="")
  fileName = paste(fileName, gsub(" ", "_", Sys.time()), sep="_")
  fileName = paste(fileName, ".tsv", sep="")
  write.table(knn_smoothing_output, fileName, sep="\t", col.names=F, row.names=T, quote=F)

}

