#!/bin/bash

# run this script using 
# $source runtimeEvaluation.sh
# otherwise, conda will not activate

timeFile="/home/rstudio/shared_pipeline_data/startAndEndTimes.txt"
inputFileTsv="/home/rstudio/shared_pipeline_data/datasets/FACS_Diaphragm_annotated_cleaned.txt"
inputFileCsv="/home/rstudio/shared_pipeline_data/datasets/FACS_Diaphragm_annotated_cleaned.csv"
outputDirectory="/home/rstudio/shared_pipeline_data/one-shot-outputs"

coolOff () {
	echo "Resuming at `date -d "+ 1 minutes" '+%Y-%m-%d %H:%M:%S'`..." 
	sleep 1m
}

logStart () {
	echo "Starting $1 on `date '+%Y-%m-%d at %H:%M:%S'`..." >> $timeFile
}

logCompletion () {
	echo "Finished $1 on `date '+%Y-%m-%d at %H:%M:%S'`."  >> $timeFile
}

#logStart ALRA 
#Rscript /home/rstudio/shared_pipeline_data/run-scripts/Rscripts/one-shot/runAlra.R $inputFileTsv $outputDirectory
#logCompletion ALRA 
#coolOff

#conda activate dca
#logStart DCA 
#/opt/miniconda3/envs/dca/bin/dca $inputFileCsv $outputDirectory/DCA
#logCompletion DCA 
#coolOff

#conda activate deepImpute
#logStart DeepImpute
#/opt/miniconda3/envs/deepImpute/bin/deepImpute $inputFileCsv --cell-axis columns --output $outputDirectory/DeepImpute
#logCompletion DeepImpute 
#coolOff

#logStart kNN-smoothing
#Rscript /home/rstudio/shared_pipeline_data/run-scripts/Rscripts/one-shot/runKnnSmoothing.R $inputFileTsv $outputDirectory
#logCompletion kNN-smoothing
#coolOff

#logStart MAGIC
#Rscript /home/rstudio/shared_pipeline_data/run-scripts/Rscripts/one-shot/runMagic.R $inputFileTsv $outputDirectory
#logCompletion MAGIC
#coolOff

#logStart SAVER
#Rscript /home/rstudio/shared_pipeline_data/run-scripts/Rscripts/one-shot/runSaver.R $inputFileTsv $outputDirectory
#logCompletion SAVER
#coolOff

#logStart SAVER-X
#Rscript /home/rstudio/shared_pipeline_data/run-scripts/Rscripts/one-shot/runSaverX.R $inputFileCsv $outputDirectory
#logCompletion SAVER-X
#coolOff

#conda activate scvi
#logStart scVI
#python3 /home/rstudio/shared_pipeline_data/run-scripts/python/runScvi.py $inputFileCsv $outputDirectory
#logCompletion scVI
#coolOff

conda activate scIGANs
echo "scIGANs has to be done separately"
logStart scIGANs
/home/rstudio/shared_pipeline_data/Tools/scIGANs/scIGANs-altered $inputFileTsv -o $outputDirectory
logCompletion scIGANs
