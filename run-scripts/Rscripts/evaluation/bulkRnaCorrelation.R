#!/usr/bin/env Rscript

library(stringr)
library(data.table)
#source('/home/s8anhegg/run-scripts/Rscripts/frequentlyUsedFunctions.R')
source('/home/rstudio/shared_pipeline_data/run-scripts/Rscripts/frequentlyUsedFunctions.R')
#source('/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/run-scripts/Rscripts/frequentlyUsedFunctions.R')

loadFile <- function(path){
  
  if (tools::file_ext(path) == "csv")
    readIn = read.csv(path, row.names = 1)
  else
    readIn = read.table(path, sep = "\t", row.names=1)
  
  # deepImpute outputs a (cell x gene) matrix instead of a (gene x cell)
  if (grepl("DeepImpute", basename(path), ignore.case=TRUE)) 
    readIn = t(readIn)
  
  # dca actually retains the cell names
  if (grepl("DCA", basename(path), ignore.case=TRUE)) {
    colnames(readIn) = as.character(readIn[1, ])
    readIn <- readIn[-1,]
  }
  
  dimensions = dim(readIn)
  print(paste("(", Sys.time(), ")"," Loaded ", basename(path), ", which details ", dimensions[1], " genes and ", dimensions[2], " cells" , sep=""))
  print("Example gene names: ")
  print(rownames(readIn)[1:3])
  print("Example cell names:")
  print(colnames(readIn)[1:3])
  
  return(readIn)
}

getMatchingOrgan <- function(filename, allOrgans){
  
  filenameSplit = tolower(str_split(baseNameNoExtension(filename), "_"))
  
  for ( j in 1:length(allOrgans)){
    
    organName = tolower(allOrgans[j])
    
    if (grepl(organName, filenameSplit))
      return(allOrgans[j])
  }
    
  
  return(NULL)
  
}

main <- function(bulkRnaSeqLocation, imputationDirectory, outputDirectory, outputTag){
  
  # bulk <- read.table(bulkRnaSeqLocation ,header=TRUE, sep = "\t",row.names=NULL, check.names=F)
  bulk = read.csv(bulkRnaSeqLocation, row.names = 1, header= TRUE)
  
  # set gene names to upper-case for string matching
  rownames(bulk) = toupper(rownames(bulk))
  
  # make alphabetical according to gene names
  bulk = bulk[order(rownames(bulk)), ]
  
  # list all input directories, excluding the imputation root directory
  inputDirectories = list.dirs(path = imputationDirectory, full.names = TRUE)
  inputDirectories = inputDirectories[!inputDirectories %in% imputationDirectory]
  
  outputDirectories = list.dirs(path = outputDirectory, full.names = TRUE)
  outputDirectories = outputDirectories[!outputDirectories %in% outputDirectory]
  
  for (d in 1:length(inputDirectories)){
    
  print(paste("Checking", basename(inputDirectories[d]), "directory for pre-existing outputs..."))
  
  currentOutputDirectory = NULL
    
  if (any(grepl(basename(inputDirectories[d]), basename(outputDirectories)))){
    # get pre-existing outputs
    currentOutputDirectory = file.path(outputDirectory, basename(inputDirectories[d]))
    filenames = getNovelInputs(inputDirectories[d], currentOutputDirectory, "*")
    print(paste(length(filenames), " might still need to be run.", sep=""))} 
  
  else{
      print(paste("None found. Making new output directory for", basename(inputDirectories[d])))
      outputFolder = file.path(outputDirectory, basename(inputDirectories[d]))
      dir.create(outputFolder)
      currentOutputDirectory = outputFolder
      filenames = dir(inputDirectories[d], full.names = TRUE)
    }
  
  if (length(filenames)==0)
    next
  
  
  for(i in 1:length(filenames)){
    
    # infer the scRNA-seq organ from filename -- assume names split using "_"
    # e.g., "FACS_Bladder_annotated_cleaned.csv"
    selectedOrgan = getMatchingOrgan(filenames[i], colnames(bulk))
    
    if (is.null(selectedOrgan)){
      #print(paste("No matching bulk RNA-seq data found for ", baseNameNoExtension(filenames[i]), ". Skipping correlation test!", sep = ""))
      next
    }
    
    print(paste("(", Sys.time(), ")"," Matched ", basename(filenames[i]), " with ", selectedOrgan, " bulk data. Loading...",sep=""))

    # load in, then make row names upper-case
    if (file.info(filenames[i])$size == 0)
      next
    
    organScRnaSeq = loadFile(filenames[i])
    rownames(organScRnaSeq) = toupper(rownames(organScRnaSeq))
    # sort scRNA-seq alphabetically by rows
    organScRnaSeq = organScRnaSeq[order(rownames(organScRnaSeq)), ]

    # drop all non-mutual genes from imputation and bulk; also, drop unmatched organs from bulk
    sharedGenes = intersect(rownames(bulk), rownames(organScRnaSeq))
    organScRnaSeq = organScRnaSeq[sharedGenes, ]
    bulkOverlap = bulk[sharedGenes, selectedOrgan]

    # initialize a C-length evaluation vector, with each element being a given cell's Rho
    correlationScores = data.frame(matrix(0, nrow = ncol(organScRnaSeq), ncol = 1))
    rownames(correlationScores) = colnames(organScRnaSeq)
    colnames(correlationScores) = c("spearman rho")

    # iterate through all scRNA-seq cells, performing spearman correlation
    print(paste("(", Sys.time(), ")", "Performing correlation..."))
    missedCounts = 0
    for (j in 1:ncol(organScRnaSeq)){

    cellName = colnames(organScRnaSeq)[j]

    tryCatch(
      {
      corr <- cor.test(as.numeric(organScRnaSeq[,cellName]), as.numeric(bulkOverlap), method = 'spearman', exact=FALSE)

      # load correlation scores
      correlationScores[cellName, "spearman rho"] = corr$estimate
      #correlationScores["p-value", cellName] = corr$p.value
      },
      error = function(e){missedCounts = missedCounts + 1}
    )
    }

    if (missedCounts > 0)
      print(paste(missedCounts, " genes(s) (", missedCounts / ncol(organScRnaSeq) * 100,  "%) could not be computed", sep = ""))
    
    
    print(paste("(", Sys.time(), ")", " Done. Printing correlation...", sep=""))
    outputName = paste(file.path(currentOutputDirectory,baseNameNoExtension(filenames[i])), "_", outputTag, ".txt", sep="")
    write.table(correlationScores, outputName, sep="\t", col.names=T, row.names=T)
  }
  }
}
  
outputTag <- "bulk-correlation"

#bulkRnaSeqLocation = "/home/s8anhegg/datasets/bulkRnaProfile.csv"
#imputationDirectory = "/home/s8anhegg/datasets/imputed-outputs/"
#outputDirectory = "/home/s8anhegg/datasets/bulk-correlation/"  

bulkRnaSeqLocation = file.path("/home/rstudio/shared_pipeline_data/Datasets/bulk-RNA/bulkRnaProfile.csv")
imputationDirectory = file.path("/home/rstudio/shared_pipeline_data/Datasets/14Dec2021_datasets-structure/bulk-correlation/second-run/magic")
outputDirectory = file.path("/tmp/") #file.path("/home/rstudio/shared_pipeline_data/Datasets/really-shortened/output-directory/")

#bulkRnaSeqLocation = file.path("/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/bulk-RNA/bulkRnaProfile.csv")
#imputationDirectory = file.path("/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/datasets-structure/imputed-outputs/")
#outputDirectory = file.path("/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/datasets-structure/bulk-correlation/")

main(bulkRnaSeqLocation, imputationDirectory, outputDirectory, outputTag)
