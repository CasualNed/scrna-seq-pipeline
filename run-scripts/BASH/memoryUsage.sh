#!/bin/bash

outputfile="/home/rstudio/shared_pipeline_data/one-shot-outputs/memoryUsage.csv"

echo "date, time, memory (MB), total memory (MB), memory use (%), disk use (GB), total disk (GB), disk use (%), CPU load" >> $outputfile 

while true; do
  echo -n `date '+%Y-%m-%d, %H:%M:%S,'`
  free -m | awk 'NR==2{printf " %s, %s, %.2f,", $3,$2,$3*100/$2 }' # memory
  df -h | awk '$NF=="/"{printf " %d, %d, %s,", $3,$2,$5}' | tr -d '%' # disk. tr drops percentage sign
  top -bn1 | grep load | awk '{printf "%.2f", $(NF-2)}' # cpu
  echo "" # for newline
  sleep 5s
done >> $outputfile
