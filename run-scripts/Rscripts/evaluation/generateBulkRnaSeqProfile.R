#!/usr/bin/env Rscript
# Hou et al. define their bulk RNA-seq profile as being "averaged across replicate bulk samples"
# generate profiles according to annotation file's "source.name" column alone,
# since Hou's scRNA-seq data did not mention age, nor sex.
generateBulkRnaSeqProfile <-function(bulkLocation, annotationLocation){
  data = read.csv(bulkLocation, row.names = 1, header= TRUE)
  annotations = read.csv(annotationLocation, row.names = 1, header= TRUE)
  # get all unique organ names from annotation's "source.name" column, dropping anything following an underscore
  names <- unique(gsub("\\_.*","", annotations$source.name))
  # make an empty RNA-seq profile with columns as organ, and rows as genes
  rnaSeqProfiles = data.frame(matrix(0, nrow = length(rownames(data)), ncol = length(names)))
  colnames(rnaSeqProfiles) = names
  rownames(rnaSeqProfiles) = rownames(data)
  # make a dataframe to keep track of the number of previous organ additions
  numberOfPreviousAdditions = data.frame(matrix(0, nrow = 1, ncol = length(names)))
  colnames(numberOfPreviousAdditions) = names
  # determine which organ a given row belongs to, then contribute its gene expression to its average
  for (mouseName in colnames(data)){
    # use gsub() to drop .gencode.xxxx from mouseName -- annotations doesn't use these
    organType <- gsub("\\_.*","", annotations[gsub("\\..*","",mouseName), "source.name"])
    # new addition gets (1/previousAdditions) of the weight
    weight <- c(numberOfPreviousAdditions[1,organType], 1)
    # use weighted average to integrate mouseName's data with running average
    # TODO: don't overwrite the column names of rnaSeqProfiles
    rnaSeqProfiles[,organType] <- apply(cbind(rnaSeqProfiles[,organType], data[,mouseName]), 1, function(d) weighted.mean(d, weight, na.rm = TRUE))
    # increase weight
    numberOfPreviousAdditions[1,organType] = numberOfPreviousAdditions[1,organType] + 1
  }
  write.table(rnaSeqProfiles, "bulkRnaProfile.csv", sep=",", col.names=T, row.names=T, quote=F)
}
args <- commandArgs(trailingOnly = TRUE)
if (length(args) == 2){
  produceDownsamples(args[1], args[2])
}else{
  print("Please provide the location of the RNA-seq profile, and the annotation location")
}
generateBulkRnaSeqProfile("/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/bulk-RNA/GSE132040_190214_A00111_0269_AHH3J3DSXX_190214_A00111_0270_BHHMFWDSXX.csv",
                          "/home/anthony/Documents/Bioinformatics/Thesis/scRNA-seq-pipeline/Datasets/bulk-RNA/GSE132040_MACA_Bulk_metadata.csv")